Motor controller that communicates via Modbus over RS232. We want to make a simple battery meter by getting the values of state of charge on the motor controller.

This is to be graphically represented as a battery meter energy on the OLED display.

You will need to use MODBUS related libraries for Arduino and the Adafruit OLED display driver / libary for the OLED display.

We are using the WEMOS with OLED display.
