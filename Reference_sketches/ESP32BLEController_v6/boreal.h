#define boreal_width 64
#define boreal_height 64

const uint8_t boreal_bits[] PROGMEM = {
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0C, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x1C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x1C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1C, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x38, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 0x10, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x78, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x78, 0x60, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x78, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0xF8, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF8, 0xC0, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0xF0, 0xC0, 0x01, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0xF0, 0x81, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF0, 0x81, 0x03, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0xF0, 0x81, 0x03, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0xF0, 0x03, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF0, 0x03, 0x07, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0xF0, 0x03, 0x0F, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0xF0, 0x03, 0x0E, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x07, 0x1E, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x07, 0x3E, 0x04, 0x00, 0x00, 0x00, 
  0x00, 0xE0, 0x07, 0x3C, 0x08, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x0F, 0x7C, 
  0x10, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x0F, 0x7C, 0x30, 0x00, 0x00, 0x00, 
  0x00, 0xE0, 0x0F, 0xF8, 0x60, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x1F, 0xF8, 
  0xC0, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x1F, 0xF8, 0xC1, 0x01, 0x00, 0x00, 
  0x00, 0xC0, 0x1F, 0xF0, 0x81, 0x03, 0x00, 0x00, 0x00, 0xC0, 0x1F, 0xF0, 
  0x03, 0x07, 0x00, 0x00, 0x00, 0xC0, 0x3F, 0xF0, 0x03, 0x07, 0x00, 0x00, 
  0x00, 0xC0, 0x3F, 0xE0, 0x07, 0x0E, 0x00, 0x00, 0x00, 0xC0, 0x3F, 0xE0, 
  0x07, 0x1C, 0x00, 0x00, 0x00, 0xE0, 0x7F, 0xE0, 0x0F, 0x3C, 0x00, 0x00, 
  0x00, 0xE0, 0x7F, 0xC0, 0x0F, 0x78, 0x00, 0x00, 0x00, 0xE0, 0x3F, 0xC0, 
  0x1F, 0xF0, 0x00, 0x00, 0x00, 0xE0, 0x1F, 0xC0, 0x1F, 0xF0, 0x01, 0x00, 
  0x00, 0xF0, 0x0F, 0xE0, 0x3F, 0xE0, 0x03, 0x00, 0x00, 0xF0, 0x07, 0xF0, 
  0x7F, 0xC0, 0x07, 0x00, 0x00, 0xF0, 0x03, 0xF8, 0x1F, 0xC0, 0x0F, 0x00, 
  0x00, 0xF8, 0x01, 0xFC, 0x03, 0x80, 0x1F, 0x00, 0x00, 0xF8, 0x00, 0xFE, 
  0x00, 0x80, 0x3F, 0x00, 0x00, 0x78, 0x00, 0x3F, 0x00, 0xF8, 0x7F, 0x00, 
  0x00, 0x3C, 0x80, 0x07, 0x00, 0xFF, 0x7F, 0x00, 0x00, 0x1C, 0xE0, 0x01, 
  0xE0, 0x1F, 0x00, 0x00, 0x00, 0x0C, 0x70, 0x00, 0x08, 0x00, 0x00, 0x00, 
  0x00, 0x04, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, };
