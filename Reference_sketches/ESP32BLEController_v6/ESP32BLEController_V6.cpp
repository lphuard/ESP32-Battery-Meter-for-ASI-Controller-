
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>
#include <MPU9250_asukiaaa.h>
#include "SSD1306.h" // alias for `#include "SSD1306Wire.h"'
#include "boreal.h"

#define min(a,b) ((a)<(b)?(a):(b));
#define SDA_PIN 5
#define SCL_PIN 4

#define LED_CHANNEL_0     0
#define LED_CHANNEL_1     1
#define MTR_CHANNEL_0     2
#define MTR_CHANNEL_1     3
#define TIMER_13_BIT  13
#define BASE_FREQ     5000
#define LED_PIN_0            13
#define LED_PIN_1            15
#define MTR_PIN_0            12
#define MTR_PIN_1            14

int brightness = 0;
int fadeAmountLED = 5;
int intense = 0;
int fadeAmountMTR = 80;
int loopLED = 3;
int loopMTR = 3;

HardwareSerial Serial1(1);
HardwareSerial Serial2(2);

SSD1306  display(0x3c, SDA_PIN, SCL_PIN);

MPU9250 mySensor;



BLEServer *pServer = NULL;
BLECharacteristic * pTxCharacteristic;
bool deviceConnected = false;
bool oldDeviceConnected = false;

#define   protocolSize  100  //Maximum Size of Protocl Bytes to be recieved via UART
#define   MAXARGC   20
char   uartBuffer[protocolSize];
int     argc;
char     *argv[ MAXARGC ];
int   splitCommandLine  ( void ); //



#define noCase              0
#define LMB                 1
#define LLB                 2
#define LML 3
#define RMB 4
#define RLB 5
#define RML 6
uint8_t currentCase;

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/

#define SERVICE_UUID           "6E400001-B5A3-F393-E0A9-E50E24DCCA9E" // UART service UUID
#define CHARACTERISTIC_UUID_RX "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_TX "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"


class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
      Serial.println("Device Connected");
    };

    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
      Serial.println("Device Disconnected");
    }
};

int event_type = 0;
int duration;
int strength, strengthmap;
int interval;
int repetition;
uint8_t currentCounter = 0;
unsigned int len;

String temp = "";
class MyCallbacks: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic)
    {
      std::string value = pCharacteristic->getValue();

      if (value.length() > 0) 
      {

        for (int i = 0; i < value.length(); i++) {
          temp += value[i];
        }
        temp.toCharArray(uartBuffer, value.length());
        temp="";
        Serial.println(uartBuffer);
        int totalarg = splitCommandLine();
        // Serial.print("Total Arg: ");
        //Serial.println(totalarg);

        if (currentCase == noCase)
        {


          // len = temp.length();
          Serial.print("current event is ");
          Serial.println(argv[0]);
          Serial.print("duration is ");
          duration = atoi(argv[1]);
          Serial.println(duration);
          Serial.print("strength is ");
          strengthmap = atoi(argv[4]);
          strength = map(strengthmap, 0, 100, 0, 255);
          Serial.println(strengthmap);
          Serial.print("interval is ");
          interval = atoi(argv[2]);
          Serial.println(interval);
          Serial.print("repetition is ");
          repetition = atoi(argv[3]);
          Serial.println(repetition);

          if (strcmp(argv[0], "LMB") == 0)
          {
            Serial.println("Left Motor");
            currentCase = LMB;
          }
          else if (strcmp(argv[0], "LLB") == 0)
          {
            Serial.println("Left LED");
            currentCase = LLB;
          }
          else if (strcmp(argv[0], "LML") == 0)
          {
            Serial.println("Left Motor and LED");
            currentCase = LML;
          }
          else if (strcmp(argv[0], "RMB") == 0)
          {
            Serial.println("Right Motor");
            currentCase = RMB;
          }
          else if (strcmp(argv[0], "RLB") == 0)
          {
            Serial.println("Right Led");
            currentCase = RLB;
          }
          else if (strcmp(argv[0], "RML") == 0)
          {
            Serial.println("Right Motor and LED");
            currentCase = RML;
          }

        }
        else
        {
          Serial.println("Event is in progress so ignore");
        }
        temp = "";

      }
    }


};
void ledcAnalogWriteLED(uint8_t channela, uint32_t value, uint32_t valueMax = 255) {
  uint32_t duty = (8191 / valueMax) * min(value, valueMax);
  Serial.print("Channel: ");
  Serial.println(channela);
  ledcWrite(channela, duty);
}
void ledcAnalogWriteMTR(uint8_t channelb, uint32_t value, uint32_t valueMax = 255) {
  uint32_t duty = (8191 / valueMax) * min(value, valueMax);
  Serial.print("Channel: ");
  Serial.println(channelb);
  ledcWrite(channelb, duty);
}

void setup() {

  while (!Serial);

  display.init();
  display.flipScreenVertically();
  display.setColor(WHITE);

  Serial.begin(115200);
  Serial1.begin (115200);
  Serial2.begin (115200);

  Serial1.println("This message is sent to UART1");
  Serial2.println("This message is sent to UART2");

  Wire.begin(SDA_PIN, SCL_PIN);
  mySensor.setWire(&Wire);
  mySensor.beginAccel();
  mySensor.beginMag();




  // Setup timer and attach timer to a led pin
  ledcSetup(LED_CHANNEL_0, BASE_FREQ, TIMER_13_BIT);
  ledcAttachPin(LED_PIN_0, LED_CHANNEL_0);
  ledcSetup(LED_CHANNEL_1, BASE_FREQ, TIMER_13_BIT);
  ledcAttachPin(LED_PIN_1, LED_CHANNEL_1);
  //Motors
  ledcSetup(MTR_CHANNEL_0, BASE_FREQ, TIMER_13_BIT);
  ledcAttachPin(MTR_PIN_0, MTR_CHANNEL_0);
  ledcSetup(MTR_CHANNEL_1, BASE_FREQ, TIMER_13_BIT);
  ledcAttachPin(MTR_PIN_1, MTR_CHANNEL_1);

  // Create the BLE Device
  BLEDevice::init("UART Service");

  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic
  pTxCharacteristic = pService->createCharacteristic(
                        CHARACTERISTIC_UUID_TX,
                        BLECharacteristic::PROPERTY_NOTIFY
                      );

  pTxCharacteristic->addDescriptor(new BLE2902());

  BLECharacteristic * pRxCharacteristic = pService->createCharacteristic(
      CHARACTERISTIC_UUID_RX,
      BLECharacteristic::PROPERTY_WRITE
                                          );

  pRxCharacteristic->setCallbacks(new MyCallbacks());

  // Start the service
  pService->start();

  // Start advertising
  pServer->getAdvertising()->start();
  Serial.println("Waiting a client connection to notify...");


}
long lastEventTime;

void loop()
{

  mySensor.accelUpdate();
 // Serial.print("accelX: " + String(mySensor.accelX()));
 // Serial.print("  accelY: " + String(mySensor.accelY()));
 // Serial.print("  accelZ: " + String(mySensor.accelZ()));
 // Serial.println("  accelSqrt: " + String(mySensor.accelSqrt()));
  mySensor.magUpdate();
  //Serial.print("  magX: " + String(mySensor.magX()));
  //Serial.print("  maxY: " + String(mySensor.magY()));
  //Serial.print("  magZ: " + String(mySensor.magZ()));
  //Serial.print("  horizontal direction: " + String(mySensor.magHorizDirection()));
  //Serial.println("at " + String(millis()) + "ms");

  drawLogo();

if(currentCase > noCase)
{
  //Serial.print("Current Case: ");
   // Serial.println(currentCase);
    
  switch (currentCase)
  {
    case LMB:
      Left_Motor_ON();
      currentCase = noCase;
      break;
    case LLB:
      Left_LED_ON();
      currentCase = noCase;
      break;
    case LML:
      Left_Motor_LED_ON();
      currentCase = noCase;
      break;
    case RMB:
      Right_Motor_ON();
      currentCase = noCase;
      break;
    case RLB:
      Right_LED_ON();
      currentCase = noCase;
      break;
    case RML:
      Right_Motor_LED_ON();
      currentCase = noCase;
      break;
    default:
      break;
  }
}





}



void Left_LED_ON( )

{
  for (int i = 0; i < repetition; i++)
  {

    ledcAnalogWriteLED(LED_CHANNEL_1, strength);
    delay(duration);
    ledcAnalogWriteLED(LED_CHANNEL_1, LOW);
    delay(interval);
  }



}
void Right_LED_ON()
{
  for (int i = 0; i < repetition; i++)
  {
    ledcAnalogWriteLED(LED_CHANNEL_0, strength);
    delay(duration);
    ledcAnalogWriteLED(LED_CHANNEL_0, LOW);
    delay(interval);
  }

}

void Right_Motor_ON()
{
  for (int i = 0; i < repetition; i++)
  {
    ledcAnalogWriteMTR(MTR_CHANNEL_0, strength);
    delay(duration);
    ledcAnalogWriteMTR(MTR_CHANNEL_0, LOW);
    delay(interval);
  }


}

void Left_Motor_ON()
{
  for (int i = 0; i < repetition; i++)
  {
    ledcAnalogWriteMTR(MTR_CHANNEL_1, strength);
    delay(duration);
    ledcAnalogWriteMTR(MTR_CHANNEL_1, LOW);
    delay(interval);
  }


}

void Right_Motor_LED_ON()
{
  for (int i = 0; i < repetition; i++)
  {
    ledcAnalogWriteLED(LED_CHANNEL_0, strength);
    ledcAnalogWriteMTR(MTR_CHANNEL_0, strength);
    delay(duration);
    ledcAnalogWriteLED(LED_CHANNEL_0, LOW);
    ledcAnalogWriteMTR(MTR_CHANNEL_0, LOW);
    delay(interval);
  }

}

void Left_Motor_LED_ON()
{
  for (int i = 0; i < repetition; i++)
  {
    ledcAnalogWriteMTR(MTR_CHANNEL_1, strength);
    ledcAnalogWriteLED(LED_CHANNEL_1, strength);
    delay(duration);
    ledcAnalogWriteMTR(MTR_CHANNEL_1, LOW);
    ledcAnalogWriteLED(LED_CHANNEL_1, LOW);
    delay(interval);
  }

}

int   splitCommandLine  ( void )
{
  int i;
  char *result = strtok( uartBuffer, ",\r\n" );
  argc = 0;
  for ( i = 0; i < MAXARGC; i++ )
  {
    if ( result )
      argc++;
    argv[ i ] = result;
    result = strtok( NULL, ",\r\n" );
  }
  return argc;
}



void drawLogo() {
  display.clear();
  display.drawXbm(42, 0, boreal_width, boreal_height, boreal_bits);
  display.setFont(ArialMT_Plain_10);
  display.drawString(0, 1, "X: " + String(mySensor.accelX()));
  display.drawString(0, 11, "Y: " + String(mySensor.accelY()));
  display.drawString(0, 21, "Z: " + String(mySensor.accelZ()));
  display.drawString(0, 31, "O: " + String(mySensor.accelSqrt()));
  display.drawString(0, 41, "LED: " + String(brightness));
  display.drawString(0, 51, "MTR: " + String(intense));
  display.display();

}


